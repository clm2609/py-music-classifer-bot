# Classify music bot
## Usage

* \help will display available commands

## Installation

```bash
sh init.sh
```

## Execution

### Requirements

* Python 3.6
* pip
* pipenv

### Execute

copy startbot.mock.sh to startbot.sh:

```bash
cp startbot.mock.sh startbot.sh
```

Change the key values to match you keys.

Execute normally.

```bash
sh startbot.sh
```

## Troubleshooting

pipenv fails with pip 18.1 (12/10/2018):

```bash
pip install pipenv
pipenv run pip install pip==18.0
pipenv install
```