from peewee import *

database = SqliteDatabase("musicClassifier.db")

class BaseModel(Model):
    class Meta:
        database = database

class Artists(BaseModel):
    artist = CharField()
    chat = CharField()

class Genres(BaseModel):
    genre = CharField()
    chat = CharField()

def create_tables():
    Artists.create_table(True)
    Genres.create_table(True)

def saveArtist(artist, genres, chat):
    try:
        art = Artists.get(
        (Artists.artist == artist) &
        (Artists.chat == chat))
    except Artists.DoesNotExist:
        art = Artists.create(
            artist = artist,
            chat = chat)
    for genre in genres:
        try:
            art = Genres.get(
            (Genres.genre == genre) &
            (Genres.chat == chat))
        except Genres.DoesNotExist:
            art = Genres.create(
                genre = genre,
                chat = chat)    

def getData(chat):
    genres = Genres.select().where(Genres.chat == chat).order_by(Genres.genre)
    artists = Artists.select().where(Artists.chat == chat).order_by(Artists.artist)
    return artists, genres

create_tables()
database.connect(reuse_if_open=True)



