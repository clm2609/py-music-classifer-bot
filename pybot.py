#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Usage:
Send music to classify it and list it.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import database
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import os

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text('''Welcome to this bot. Use /help to know more.''')
def help(bot, update):
    """Send a message when the command /help is issued."""
    update.message.reply_text('''Upload music to get the genre of what you are listening to.
    /list will list all the artists and genres you have uploaded so far.
    /help will show this message.''')

#Memes
# def toso(bot, update):
#     update.message.reply_text('Estas como un tuppy.')
# def cris(bot, update):
#     update.message.reply_text('😘')
# def andres(bot, update):
#     update.message.reply_text('Hijos de puta')
# def lobo(bot, update):
#     update.message.reply_text('😅 encontraste mi huevo de pascua.')
# def kuro(bot, update):
#     update.message.reply_text('Joer, por que pie me llegaba')

def listMusic(bot, update):
    """List artists and genres"""
    chat = update.message.chat.id
    artists, genres = database.getData(chat)
    art="Artists:\n"
    gen="Genres:\n"
    for artist in list(artists):
        art+= "#"+artist.artist+"\n"
    for genre in list(genres):
        gen += "#"+genre.genre+"\n"
    update.message.reply_text(art+gen)

def song(bot, update):
    """Classify the song"""
    chat = update.message.chat.id
    result = getGenres(update.message.audio.performer.lower())
    result = list(map(lambda x: x.replace(" ","_").replace("-","_"),result))
    genres = ""
    for genero in result:
        genres += " #"+genero
    artist = update.message.audio.performer.capitalize().replace(" ","_").replace("&","and")
    song = update.message.audio.title
    database.saveArtist(artist, result, chat)
    update.message.reply_text("#"+artist +" - "+song+genres, 
        reply_to_message_id=update.message.message_id)

def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

def getGenres(artistName):
    client_credentials_manager = SpotifyClientCredentials()
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    genres = []

    results = sp.search(artistName, type='artist')
    generos = results['artists']['items'][0]['genres']
    for genero in generos:
        genres.append(genero)
    return genres

def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(os.environ["TELEGRAM_KEY"])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("list", listMusic))
    # dp.add_handler(CommandHandler("toso", toso))
    # dp.add_handler(CommandHandler("cris", cris))
    # dp.add_handler(CommandHandler("andres", andres))
    # dp.add_handler(CommandHandler("lobo", lobo))
    # dp.add_handler(CommandHandler("kuro", kuro))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.audio, song))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
